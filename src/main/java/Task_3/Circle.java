package Task_3;

public class Circle extends GeometricFigure{
        @Override
    public double getArea(double radius) {
        if(radius<=0){
            throw new IllegalArgumentException ("Radius < 0");
        }
        return Math.PI*radius*radius;
    }
    @Override
    public double getPerimeter(double radius) {
        if(radius<=0){
            throw new IllegalArgumentException ("Radius < 0");
        }
        return 2*Math.PI*radius;
    }
}
