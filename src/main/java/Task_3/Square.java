package Task_3;
public class Square extends GeometricFigure {
        @Override
    public double getArea(double side) {
            if(side<=0){
                throw new IllegalArgumentException ("Side < 0");
            }
            return side*side;
    }
    @Override
    public double getPerimeter(double side) {
        if(side<=0){
            throw new IllegalArgumentException ("Side < 0");
        }
            return 4*side;
    }
}
