package MyTest;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataProvTest {
    @DataProvider(name = "data-provider")
        public Object[][] dataprovider() {
            return new Object[][]{
                    {"Halo Headlights"},
                    {"Projector Headlights"},
                    {"Head"},
                    {"U-Bar Headlights"},
                    {"DRL-Bar Headlights"}
            };
        }
        @Test (dataProvider = "data-provider")
        public void getLength(String line){
        Assert.assertTrue(line.length()>=10,"Good");
        }
        @Test (dataProvider = "data-provider")
        public void getLength2(String line){
         Assert.assertTrue(line.length()<10,"Bad");
        }
    }